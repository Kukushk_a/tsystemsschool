package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param //statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    public String evaluate(String statement) {

        // testing if the statement can be evaluated:

        // checking if the input string isn't empty
        if(statement==null) {
            return null;
        }
        if(statement.equals("")) {
            return null;
        }
        // checking if there is a coma in the string
        if(statement.contains(",")) {
            return null;
        }
        // checking for the dot as the first or the last element
        if((statement.charAt(0)==46)||(statement.charAt(statement.length()-1)==46)) {
            return null;
        }

        // checking for proper quantity and order of parentheses
        int forwPar = 0;
        int backPar = 0;
        int forwIndex = 0;

        for(int i=0; i<statement.length(); i++) {
            if(statement.charAt(i)==40) {
                forwPar++;
                forwIndex = i;
            }
            else if(statement.charAt(i)==41) {
                if(i<forwIndex) {
                    return null;
                }
                backPar++;
            }

        }
        if(forwPar!=backPar) {
            return null;
        }
        // checking for operator doubles
        for(int i=0; i<statement.length(); i++) {
            if((statement.charAt(i)==43)||(statement.charAt(i)==45)||(statement.charAt(i)==42)||(statement.charAt(i)==47)||(statement.charAt(i)==46)) {
                if(statement.charAt(i)==statement.charAt(i+1)) {
                    return null;
                }
            }
        }

        // arrange digits into numbers
        ArrayList<String> arrWDigits = new ArrayList<>();
        String temp = "";
        for(int i=0; i<statement.length(); i++) {
            if(Character.isDigit(statement.charAt(i))) {
                temp=temp+statement.charAt(i);
                if(i==(statement.length()-1)) {
                    arrWDigits.add(temp);
                }
            }
            else if(statement.charAt(i)==46) {
                temp=temp+statement.charAt(i);
            }
            else {
                if(!temp.equals("")) {
                    arrWDigits.add(temp);
                    temp="";
                }
                arrWDigits.add(String.valueOf(statement.charAt(i)));

            }
        }

        // transfer into reverse polish notation
        ArrayList<String> polishArr = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        int minus =0;
        for(int i=0; i<arrWDigits.size(); i++) {
            if(arrWDigits.get(i).equals("(")) {
                stack.push(arrWDigits.get(i));
            }
            else if(arrWDigits.get(i).equals(")")) {
                if(stack.peek().equals("(")) {
                    stack.pop();
                } else {
                    while (!stack.peek().equals("(")) {
                        polishArr.add(stack.pop());
                       }
                    stack.pop();
                }
            }
            else if((arrWDigits.get(i).equals("+"))) {
                if((!stack.empty())&&((stack.peek().equals("*"))||(stack.peek().equals("/")))) {
                    polishArr.add(stack.pop());
                }
                stack.push(arrWDigits.get(i));
            }
            else if(arrWDigits.get(i).equals("-")) {
                if((!stack.empty())&&((stack.peek().equals("*"))||(stack.peek().equals("/")))) {
                    polishArr.add(stack.pop());
                }
                minus++;
            }
            else if((arrWDigits.get(i).equals("*"))||(arrWDigits.get(i).equals("/"))) {
                if((!stack.empty())&&((stack.peek().equals("*"))||(stack.peek().equals("/")))) {
                    polishArr.add(stack.pop());
                    stack.push(arrWDigits.get(i));
                }
                else {
                    stack.push(arrWDigits.get(i));
                }
            }
            else {
                if (minus != 0) {
                    polishArr.add(arrWDigits.get(i));
                    polishArr.add("-");
                    minus = 0;
                } else {
                    polishArr.add(arrWDigits.get(i));
                }
            }
        }
        while(!stack.empty()) {
            polishArr.add(stack.pop());
        }



        // counting the result
        for(int i=0; i<polishArr.size(); i++) {
            if(polishArr.get(i).equals("+")) {
                double b = Double.parseDouble(stack.pop()) + Double.parseDouble(stack.pop());
                stack.push(Double.toString(b));
            }
            else if(polishArr.get(i).equals("*")) {
                double b = Double.parseDouble(stack.pop()) * Double.parseDouble(stack.pop());
                stack.push(Double.toString(b));
            }
            else if(polishArr.get(i).equals("-")) {
                double b = Double.parseDouble(stack.pop());
                double a = Double.parseDouble(stack.pop());
                double c = a-b;
                stack.push(Double.toString(c));
            }
            else if(polishArr.get(i).equals("/")) {
                double b = Double.parseDouble(stack.pop());
                double a = Double.parseDouble(stack.pop());
                if(b==0) {
                    return null;
                } else {
                    double c = a / b;
                    stack.push(Double.toString(c));
                }
            }
            else {
                stack.push(polishArr.get(i));
            }
        }

        // formatting the result
        double result = Double.parseDouble(stack.peek());
        if(result%1 == 0) {
            long l = (long) result;
            return Long.toString(l);
        } else {
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.HALF_UP);
            String s = df.format(result).replace(',', '.');
            return s;

        }

    }

}
