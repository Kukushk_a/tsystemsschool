package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {



    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if(inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        // array in java 1.8 can't be larger than Integer.Max_Value-2
        if(inputNumbers.size()>Integer.MAX_VALUE-2) {
            throw new CannotBuildPyramidException();
        }

        long D = 8*inputNumbers.size() + 1;
        double t = (Math.sqrt(D) - 1)/2;
        int numOfRows= (int)t;
        if(t>numOfRows) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int numOfEl= numOfRows*2-1;
        int[][] arr = new int[numOfRows][numOfEl];
        int currentIndex =inputNumbers.size() - 1;
        int indent = 0;

        for(int x=numOfRows-1; x>=0; x--) {
            int m=0;
            for (int i = currentIndex , j = numOfEl - indent-1;i >= 0;i--, j = j - 2){

                if((j<0)||(m==(x+1))) {
                    indent++;
                    break;
                }
                arr[x][j] = inputNumbers.get(i);
                currentIndex = i-1;
                m++;

            }
        }

        return arr;
    }

}
