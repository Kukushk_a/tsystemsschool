package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException("x and y could not be null");
        }

        int currentIndex = 0;
        int match = 0;

        for (Object inputElement : x) {
            for (int j = currentIndex; j < y.size(); j++) {
                if (y.get(j).equals(inputElement)) {
                    currentIndex = j;
                    match++;
                    break;
                }
            }
        }

        return match == x.size();

        }

}
